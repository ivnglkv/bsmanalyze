#-------------------------------------------------
#
# Project created by QtCreator 2016-03-10T16:07:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bsm_analyze
TEMPLATE = app

INCLUDEPATH += lib/qtbsm

SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    lib/qtbsm/qtbsm.cpp \
    lib/qtbsmanalyze/qtbsmanalyze.cpp

HEADERS  += src/mainwindow.h \
    lib/qtbsm/qtbsm.h \
    lib/qtbsmanalyze/qtbsmanalyze.h

FORMS    += ui/mainwindow.ui

CONFIG += c++11
