#ifndef QTBSMANALYZE_H
#define QTBSMANALYZE_H

#include "qtbsm.h"

#include <QTime>
#include <QString>
#include <QHash>

class QtBsmAnalyze
{
public:
    QtBsmAnalyze(const QList<QtBsm> &journalsToAnalyze);

    void startAnalyzing();

private:
    QList<QtBsm> bsmJournalsList;
    QHash<quint32, Controller> controllersDict;
    QHash<quint32, Employee> employeesList;
};

class Employee
{
public:
    Employee();
    Employee(const quint32 &id);

    QString fio;
    QList<PassEvent> passEvents;
private:
    quint32 id;
};

class PassEvent
{
public:
    PassEvent();

    QTime passEventTime;
    QTime timeSpentToPass;
    quint8 numberOfTries;
};

class Controller
{
public:
    Controller(const QString &desc);

    QString description;

    QList<bsmJournalEntry> eventsList;

    PassEvent getPassEventData(const QTime &eventTime);
};

#endif // QTBSMANALYZE_H
