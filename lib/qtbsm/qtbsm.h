#ifndef QTBSM_H
#define QTBSM_H

#include <QString>
#include <QList>

enum BS_LOG_EVENT : quint8
{
    BS_EV_NOT_RESPONED  = 0x01, //контроллер не отвечает

    BS_EV_USER_ADD       = 0x17, //пользователь добавлен
    BS_EV_USER_ADD_FAIL  = 0x18, //ошибка добавления пользователя

    BS_EV_WEIG_SCHED_ERROR   = 0x24, //ошибка временного режима
    BS_EV_WEIG_SCHED_DENIED  = 0x25, //отказ входа по карте согласно временному режиму
    BS_EV_IDENT_WEIG_LOCKED  = 0x26, //отказ входа по карте (сотрудник заблокирован)
    BS_EV_IDENT_WEIG_SUCCESS = 0x28, //идентификация по карте успешна
    BS_EV_IDENT_WEIG_FAILED  = 0x27, //идентификация по карте неудачна
    BS_EV_WEIG_HOL_DENIED    = 0x29, //отказ входа по карте (праздник)

    BS_EV_WEIG_HOL_LOCKED       = 0x30, //праздник заблокирован
    BS_EV_MAGNETIC_CARD_ERROR   = 0x31, //ошибка идентификации по магнитной карте
    BS_EV_MAGNETIC_CARD_SUCCESS = 0x32, //идентификация по магнитной карте успешна
    BS_EV_MAGNETIC_CARD_FAILED  = 0x33, //идентификация по магнитной карте неудачна

    BS_EV_BAD_FINGER    = 0x36, //отпечаток низкого качества
    BS_EV_IDENT_SUCCESS = 0x37, //идентификация успешна
    BS_EV_IDENT_FAILED  = 0x38, //идентификация неудачна
    BS_EV_NOT_PASS      = 0x39, //проход не зафиксирован

    BS_EV_USER_DELETE   = 0x47, //пользователь удален
    BS_EV_USER_DEL_FAIL = 0x48, //ошибка при удалении пользователя

    BS_EV_USER_LOCK      = 0x57, //пользователь заблокирован
    BS_EV_USER_LOCK_FAIL = 0x58, //ошибка при блокировки пользователя

    BS_EV_IN1_SIGNALED      = 0x64, //сигнал на вход-1
    BS_EV_USER_UNLOCK       = 0x67, //пользователь разблокирован
    BS_EV_USER_UNLOCK_FAIL  = 0x68, //ошибка разблокировки пользователя
    BS_EV_SYSTEM_START      = 0x6A, //перезапуск системы
    BS_EV_TIME_SCHED_DENIED = 0x6D, //отказано в доступе (временной режим)
    BS_EV_TIME_SCHED_ERROR  = 0x6E, //ошибка временного режима
    BS_EV_HOLIDAY_DENIED    = 0x6F, //отказано в доступе (праздник)

    BS_EV_IDENT_LOCKED_USER = 0x70, //отказано в доступе (заблокирован)
    BS_EV_HOL_LOCKED        = 0x71, //отказано в доступе (праздник)

    BS_EV_OPEN_FROM_PC  = 0x80, //открыт с ПК
    BS_EV_CLOSE_FROM_PC = 0x81, //закрыт с ПК

    BS_EV_USER_ENTER       = 0x97, //вход пользователя
    BS_EV_USER_LEAVE       = 0x98, //выход пользователя
    BS_EV_FTR_IDENT_FAILED = 0x99, //идентификация по футронику неудачна

    BS_EV_TAMPER_SWITCH = 0xA3, //корпус вскрыт

    BS_EV_SET_USER_SCHED   = 0xB0, //временной режим назначен
    BS_EV_UNSET_USER_SCHED = 0xB1, //временной режим отменен

    BS_EV_BUR_NOT_CONNECT    = 0xB2, //ошибка связи с БУР
    BS_EV_TUBUS_NOT_CONNECT  = 0xB3, //СК-24 не подключен
    BS_EV_TUBUS_OPEN         = 0xB4, //тубус взят
    BS_EV_TUBUS_CLOSE        = 0xB5, //тубус возвращен
    BS_EV_TUBUS_OPEN_FAILED  = 0xB6, //тубус не взят
    BS_EV_TUBUS_CLOSE_FAILED = 0xB7, //тубус не возвращен
    BS_EV_TUBUS_ADD          = 0xB8, //тубус добавлен
    BS_EV_TUBUS_REMOVE       = 0xB9, //тубус удален
    BS_EV_SET_USER_TUBUS     = 0xBA, //тубус назначен
    BS_EV_UNSET_USER_TUBUS   = 0xBC, //тубус отменен
    BS_EV_GUARD_OFF          = 0xBD, //защита отключена
    BS_EV_TUBUS_NOTASSIGNED  = 0xBE, //тубус не назначен

    BS_EV_FREE_EXIT_ON         = 0xE2, //режим свободного прохода  включен
    BS_EV_FREE_EXIT_OFF        = 0xE3, //режим свободного прохода выключен
    BS_EV_DOOR_NOTCLOSED       = 0xE4, //дверь не закрыта
    BS_EV_MOD_LOCKED_BY_FINGER = 0xE5, //модуль заблокирован при попытке взлома по отпечатку
    BS_EV_MOD_LOCKED_BY_CARD   = 0xE6, //модуль заблокирован при попытке взлома по карте
    BS_EV_MOD_UNLOCKED         = 0xE7, //модуль разблокирован
    BS_EV_DOOR_NOTOPENED       = 0xE8, //дверь не открыта
    BS_EV_DOOR_CRASHED         = 0xE9, //взлом двери

    BS_EV_INITIALIZED = 0xF0, //инициализация

    BS_EV_UNKNOWN //неизвестное событие
};

#pragma pack(push, 2)
struct winSystemTime {
    quint16 nYear;
    quint16 nMonth;
    quint16 nDayOfWeek;
    quint16 nDay;
    quint16 nHour;
    quint16 nMinute;
    quint16 nSecond;
    quint16 nMilliseconds;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct bsmJournalEntry {
    quint32      nControllerAddress;
    quint8       nEventCode;
    quint8       nExecDeviceStatus;
    quint8       nReservedByte;
    winSystemTime tSystemTime;
    quint32      nUserID;
};
#pragma pack(pop)

class QtBsm
{
public:
    QtBsm(const QString &filename);

private:
    QList<bsmJournalEntry> lJournalEntries;
};

#endif // QTBSM_H
