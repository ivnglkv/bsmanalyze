#include "qtbsm.h"

#include <QFile>
#include <QIODevice>
#include <QDataStream>

QtBsm::QtBsm(const QString &filename) :
    lJournalEntries(QList<bsmJournalEntry>())
{
    QFile bsmJournalFile(filename);

    if (!bsmJournalFile.exists()) {
        return;
    }

    bsmJournalFile.open(QIODevice::ReadOnly);

    QDataStream inBsmJournalStream(&bsmJournalFile);
    inBsmJournalStream.setByteOrder(QDataStream::LittleEndian);

    bsmJournalEntry journalEntry;

    do {
        inBsmJournalStream >> journalEntry.nControllerAddress
            >> journalEntry.nEventCode
            >> journalEntry.nExecDeviceStatus
            >> journalEntry.nReservedByte
            >> journalEntry.tSystemTime.nYear
            >> journalEntry.tSystemTime.nMonth
            >> journalEntry.tSystemTime.nDayOfWeek
            >> journalEntry.tSystemTime.nDay
            >> journalEntry.tSystemTime.nHour
            >> journalEntry.tSystemTime.nMinute
            >> journalEntry.tSystemTime.nSecond
            >> journalEntry.tSystemTime.nMilliseconds
            >> journalEntry.nUserID;

        lJournalEntries.append(journalEntry);
    } while (!inBsmJournalStream.atEnd());

    bsmJournalFile.close();
}
